I deleted a repo without pushing my work.  Never again.

Use like normal rm command.  All this does is check if you're trying to delete 
a git repo with changes in it.

Checkout and add rm where ever you want, so long as you have it included in 
your PATH.  My .profile file in my home directory checks for scripts in 
$HOME/bin, might be a good spot for you as well.